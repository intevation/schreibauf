// Copyright 2018 by Siemens AG. All rights reserved.
// Engineering by Intevation GmbH.
// Use of this source code is governed by the Apache 2.0 license
// that can be found in the LICENSE file.

package main

import (
	"database/sql"
	"sort"
	"time"
)

type idDataset struct {
	id      int64
	dataset *dataset
}

type idDatasetRing struct {
	slots []idDataset
	head  int
}

func newIDDatasetRing(sz int) *idDatasetRing {
	return &idDatasetRing{
		slots: make([]idDataset, 0, sz),
	}
}

func timeTrack(start time.Time, name string) {
	elapsed := time.Since(start)
	log.Infof("%s took %s.", name, elapsed)
}

func (r *idDatasetRing) prefill(db *sql.DB, tableName string) error {

	defer timeTrack(time.Now(), "pre filling ring buffer")

	r.reset()
	p := &prepared{prefix: tableName, tmpl: bootTmpl}
	defer p.close()
	year := time.Now().Year()
	stmt, err := p.prepare(year, db)
	if err != nil {
		return err
	}

	rows, err := stmt.Query(cap(r.slots))
	if err != nil {
		return err
	}
	defer rows.Close()
	for rows.Next() {
		ids := idDataset{dataset: new(dataset)}
		if err := rows.Scan(
			&ids.id,
			&ids.dataset.eventType,
			&ids.dataset.timeNanos,
			&ids.dataset.longitude,
			&ids.dataset.latitude,
			&ids.dataset.multiplicity,
			&ids.dataset.cpCount,
		); err != nil {
			return err
		}
		r.slots = append(r.slots, ids)
	}
	return nil
}

func (r *idDatasetRing) reset() {
	for i := range r.slots {
		r.slots[i] = idDataset{}
	}
	r.slots = r.slots[:0]
	r.head = 0
}

const timeEps = time.Microsecond * 250

func (r *idDatasetRing) find(ds *dataset) *idDataset {

	before := ds.timeNanos.Add(-timeEps)
	n := len(r.slots)
	i := sort.Search(n, func(i int) bool {
		i = (r.head + i) % n // ring buffer index
		return r.slots[i].dataset.timeNanos.After(before)
	})

	if i == n {
		return nil
	}

	after := ds.timeNanos.Add(timeEps)
	for i = (r.head + i) % n; r.slots[i].dataset.timeNanos.Before(after); {
		if r.slots[i].dataset.equals(ds) {
			return &r.slots[i]
		}
		if i = (i + 1) % n; i == r.head {
			return nil
		}
	}
	return nil
}

func (r *idDatasetRing) insert(id int64, ds *dataset) {

	if n := len(r.slots); n < cap(r.slots) {
		r.slots = append(r.slots, idDataset{id, ds})
		for i := n; i > 0; i-- {
			if r.slots[i-1].dataset.timeNanos.Before(ds.timeNanos) {
				break
			}
			r.slots[i], r.slots[i-1] = r.slots[i-1], r.slots[i]
		}
	} else { // buffer is already full
		// if the new one is younger than the oldest in buffer
		// ignore it.
		if ds.timeNanos.Before(r.slots[r.head].dataset.timeNanos) {
			return
		}
		r.slots[r.head] = idDataset{id, ds}
		for i := r.head; ; {
			j := i - 1
			if j < 0 {
				j = n - 1
			}
			if j == r.head ||
				r.slots[j].dataset.timeNanos.Before(r.slots[i].dataset.timeNanos) {
				break
			}
			r.slots[i], r.slots[j] = r.slots[j], r.slots[i]
			i = j
		}
		r.head = (r.head + 1) % n
	}
}
