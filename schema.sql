--
-- A simplified database scheme to play with schreibauf.
--
-- createuser schreiber
-- createdb blitze -O schreiber
-- psql -c "ALTER USER schreiber WITH PASSWORD 'supersecret';"
-- psql -d blitze -c "CREATE EXTENSION postgis;"
-- psql -d blitze -U schreiber -h localhost -f schema.sql

BEGIN;

CREATE TABLE blitze2018 (
    id           serial PRIMARY KEY NOT NULL,
    zeit         timestamp with time zone,
    typ          smallint,
    strom        numeric,
    eintrag      timestamp with time zone DEFAULT now(),
    koordinate   geometry(Point, 4326),
    grosseachse  numeric,
    kleineachse  numeric,
    winkelachse  numeric,
    freigrade    integer,
    chiquadrat   numeric,
    steigzeit    numeric,
    spitzenull   numeric,
    sensorzahl   numeric,
    hoehe        numeric DEFAULT 0,
    tlp          numeric DEFAULT 1,
    maxraterise  numeric,
    hoeheungenau numeric,
    strokezahl   integer,
    pulsezahl    integer
);

CREATE INDEX "b_koord_2018_idx" ON blitze2018 USING gist(koordinate);
CREATE INDEX "b_zeit_2018_idx" ON blitze2018 USING btree(zeit);

COMMIT;

