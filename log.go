// Copyright 2018 by Siemens AG. All rights reserved.
// Engineering by Intevation GmbH.
// Use of this source code is governed by the Apache 2.0 license
// that can be found in the LICENSE file.

package main

import (
	"os"

	"github.com/sirupsen/logrus"
)

var log = logrus.New()

func configureLogging(cfg *config) {
	logLevel(*cfg.LogLevel)
	logFile(*cfg.LogFile)
}

func logLevel(lvl string) {
	l, err := logrus.ParseLevel(lvl)
	if err != nil {
		l = logrus.WarnLevel
	}
	log.SetLevel(l)
}

func logFile(file string) {
	if file != "" {
		f, err := os.OpenFile(file, os.O_APPEND|os.O_CREATE|os.O_WRONLY, 0666)
		if err == nil {
			log.Out = f
		} else {
			logrus.Warn("open logger-file failed")
		}
	} else {
		log.Out = os.Stderr
	}
}
