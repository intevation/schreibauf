// Copyright 2018 by Siemens AG. All rights reserved.
// Engineering by Intevation GmbH.
// Use of this source code is governed by the Apache 2.0 license
// that can be found in the LICENSE file.

package main

import (
	"errors"
	"fmt"
	"time"

	"github.com/BurntSushi/toml"
)

var (
	defaultSinkHost          = "localhost"
	defaultSinkPort          = 5432
	defaultSinkUser          = "scott"
	defaultSinkPassword      = "tiger"
	defaultSinkDatabase      = "kugelblitz"
	defaultSinkSSL           = "require"
	defaultSinkMaxReconnects = -1
	defaultSinkReconnectWait = duration{time.Second * 10}
	defaultSinkTableName     = "blitz.blitze"
	defaultSinkBufferSize    = 8 * 1024
	defaultSinkSQLDedup      = false

	defaultSourceHost          = "localhost"
	defaultSourcePort          = 8082
	defaultSourceKey           = ""
	defaultSourceKeepAlive     = duration{10 * time.Second}
	defaultSourceMaxReconnects = -1
	defaultSourceReconnectWait = duration{time.Second * 10}

	defaultLogLevel = "Error"
	defaultLogFile  = ""
)

type rewrites []struct {
	ID  int `toml:"id"`
	New int `toml:"new"`
}

func (rw rewrites) replace(id int) int {
	for i := range rw {
		if rw[i].ID == id {
			return rw[i].New
		}
	}
	return id
}

type duration struct {
	time.Duration
}

type sink struct {
	Host          *string   `toml:"host"`
	Port          *int      `toml:"port"`
	User          *string   `toml:"user"`
	Password      *string   `toml:"password"`
	Database      *string   `toml:"database"`
	SSL           *string   `toml:"ssl"`
	MaxReconnects *int      `toml:"max-reconnects"`
	ReconnectWait *duration `toml:"wait-reconnect"`
	TableName     *string   `toml:"table-name"`
	BufferSize    *int      `toml:"buffer-size"`
	Rewrites      rewrites  `toml:"rewrites"`
	SQLDedup      *bool     `toml:"sql-dedup"`
}

type source struct {
	ID            int       `toml:"id"`
	Host          *string   `toml:"host"`
	Port          *int      `toml:"port"`
	Key           *string   `toml:"key"`
	Sinks         []string  `toml:"sinks"`
	KeepAlive     *duration `toml:"keep-alive"`
	MaxReconnects *int      `toml:"max-reconnects"`
	ReconnectWait *duration `toml:"wait-reconnect"`
}

type config struct {
	Sources  map[string]*source `toml:"sources"`
	Sinks    map[string]*sink   `toml:"sinks"`
	LogLevel *string            `toml:"log-level"`
	LogFile  *string            `toml:"log-file"`
}

var errStringExpected = errors.New("expected string")

func (d *duration) UnmarshalTOML(x interface{}) error {
	s, ok := x.(string)
	if !ok {
		return errStringExpected
	}
	v, err := time.ParseDuration(s)
	if err == nil {
		*d = duration{v}
	}
	return err
}

func (s *source) fillDefaults() {
	if s.Host == nil {
		s.Host = &defaultSourceHost
	}
	if s.Port == nil {
		s.Port = &defaultSourcePort
	}
	if s.Key == nil {
		s.Key = &defaultSourceKey
	}
	if s.KeepAlive == nil {
		s.KeepAlive = &defaultSourceKeepAlive
	}
	if s.ReconnectWait == nil {
		s.ReconnectWait = &defaultSinkReconnectWait
	}
	if s.MaxReconnects == nil {
		s.MaxReconnects = &defaultSinkMaxReconnects
	}
}

func (s *sink) fillDefaults() {
	if s.Host == nil {
		s.Host = &defaultSinkHost
	}
	if s.Port == nil {
		s.Port = &defaultSinkPort
	}
	if s.Database == nil {
		s.Database = &defaultSinkDatabase
	}
	if s.User == nil {
		s.User = &defaultSinkUser
	}
	if s.Password == nil {
		s.Password = &defaultSinkPassword
	}
	if s.TableName == nil {
		s.TableName = &defaultSinkTableName
	}
	if s.BufferSize == nil {
		s.BufferSize = &defaultSinkBufferSize
	}
	if s.SSL == nil {
		s.SSL = &defaultSinkSSL
	}
	if s.ReconnectWait == nil {
		s.ReconnectWait = &defaultSinkReconnectWait
	}
	if s.MaxReconnects == nil {
		s.MaxReconnects = &defaultSinkMaxReconnects
	}
	if s.SQLDedup == nil {
		s.SQLDedup = &defaultSinkSQLDedup
	}
}

func (cfg *config) fillDefaults() {
	if cfg.Sources != nil {
		for _, s := range cfg.Sources {
			s.fillDefaults()
		}
	}
	if cfg.Sinks != nil {
		for _, s := range cfg.Sinks {
			s.fillDefaults()
		}
	}

	if cfg.LogLevel == nil {
		cfg.LogLevel = &defaultLogLevel
	}
	if cfg.LogFile == nil {
		cfg.LogFile = &defaultLogFile
	}
}

var (
	errNoSources = errors.New("no sources found")
	errNoSinks   = errors.New("no sinks found")
)

func (cfg *config) sourceByID(id int) *source {
	for _, s := range cfg.Sources {
		if s.ID == id {
			return s
		}
	}
	return nil
}

func (cfg *config) checkRewrites() error {
	for name, snk := range cfg.Sinks {
	rewrites:
		for _, rw := range snk.Rewrites {
			src := cfg.sourceByID(rw.ID)
			if src == nil {
				return fmt.Errorf(
					"sink '%s' has a rewite rule for non-existing source with id %d",
					name, rw.ID)
			}
			for _, s := range src.Sinks {
				if s == name {
					continue rewrites
				}
			}
			return fmt.Errorf(
				"sink '%s' is not target of source with id %d", name, rw.ID)
		}
	}
	return nil
}

func (cfg *config) check() error {

	if cfg.Sources == nil || len(cfg.Sources) == 0 {
		return errNoSources
	}
	if cfg.Sinks == nil || len(cfg.Sinks) == 0 {
		return errNoSinks
	}

	usedSinks := make(map[string][]int)

	for name, s := range cfg.Sources {
		if len(s.Sinks) == 0 {
			return fmt.Errorf("source '%s' has no sinks", name)
		}
		for _, snk := range s.Sinks {
			if cfg.Sinks[snk] == nil {
				return fmt.Errorf(
					"source '%s' points to not existing sink '%s'", name, snk)
			}
			us := usedSinks[snk]
			for _, id := range us {
				if id == s.ID {
					return fmt.Errorf(
						"sink has multiple sources with id %d", id)
				}
			}
			usedSinks[snk] = append(us, s.ID)
		}
	}

	return cfg.checkRewrites()
}

func loadConfig(filename string) (*config, error) {
	cfg := new(config)
	_, err := toml.DecodeFile(filename, cfg)
	if err != nil {
		return nil, err
	}
	cfg.fillDefaults()

	return cfg, cfg.check()
}
