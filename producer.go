// Copyright 2018 by Siemens AG. All rights reserved.
// Engineering by Intevation GmbH.
// Use of this source code is governed by the Apache 2.0 license
// that can be found in the LICENSE file.

package main

import (
	"bufio"
	"errors"
	"fmt"
	"io"
	"net"
	"time"
)

type producer struct {
	source    *source
	consumers []*queue

	quit chan struct{}
	done bool
}

func newProducer(src *source, consumers []*queue) *producer {
	return &producer{
		source:    src,
		consumers: consumers,
		quit:      make(chan struct{}, 2),
	}
}

func (p *producer) broadcast(ds *dataset) {
	ds.sourceID = p.source.ID
	for _, c := range p.consumers {
		c.Append(ds)
	}
}

func writeAll(w io.Writer, buf []byte) error {
	for len(buf) > 0 {
		n, err := w.Write(buf)
		if err != nil {
			return err
		}
		buf = buf[n:]
	}
	return nil
}

var errNoMoreLines = errors.New("no more lines")

func (p *producer) handle(con net.Conn) error {

	defer con.Close()

	// Only start sending tokens if we have a key.
	if *p.source.Key != "" {
		token, err := generateTokenMessage(*p.source.Key)
		if err != nil {
			return err
		}
		done := make(chan struct{})

		defer close(done)

		go func() {
			for {
				select {
				case <-done:
					return
				case <-time.After((*p).source.KeepAlive.Duration):
					if err := writeAll(con, token); err != nil {
						log.Errorf("error: %v.", err)
						return
					}
				}
			}
		}()
	}

	type line struct {
		text string
		err  error
	}

	lines := make(chan line)
	ldone := make(chan struct{})

	defer close(ldone)

	go func() {
		scan := bufio.NewScanner(con)
		for scan.Scan() {
			select {
			case <-ldone:
				return
			case lines <- line{text: scan.Text()}:
			}
		}
		err := scan.Err()
		if err == nil {
			err = errNoMoreLines
		}
		lines <- line{err: err}
	}()

	for {
		select {
		case <-p.quit:
			p.done = true
			return nil
		case l := <-lines:
			switch {
			case l.err == errNoMoreLines:
				return nil
			case l.err != nil:
				return l.err
			}
			var ds dataset
			if err := ds.parse(l.text); err != nil {
				log.Warnf("parsing failed: %v.", err)
				continue
			}
			switch ds.control {
			case dataLine:
				p.broadcast(&ds)
			case keepAliveLine:
				// Ignore silently.
			default:
				log.Warnf("ignoring message of type %d.", ds.control)
			}
		}
	}
}

func (p *producer) run() {
	addr := fmt.Sprintf("%s:%d", *p.source.Host, *p.source.Port)
	for !p.done {
		var con net.Conn
		log.Infof("Connecting to TLP %s", addr)
		for tries := 0; ; {
			select {
			case <-p.quit:
				p.done = true
			default:
			}
			if p.done {
				return
			}
			var err error
			con, err = net.Dial("tcp", addr)
			if err == nil {
				break
			}
			log.Warnf("dialing failed: %v.", err)
			if *p.source.MaxReconnects > 0 {
				if tries++; tries > *p.source.MaxReconnects {
					log.Errorf("error: producer with id %d died.", p.source.ID)
					return
				}
			}
			time.Sleep((*p).source.ReconnectWait.Duration)
		}
		if err := p.handle(con); err != nil {
			log.Errorf("error: %v.", err)
		}
	}
}
