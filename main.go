// Copyright 2018 by Siemens AG. All rights reserved.
// Engineering by Intevation GmbH.
// Use of this source code is governed by the Apache 2.0 license
// that can be found in the LICENSE file.

package main

import (
	"flag"
	lg "log"
)

func boot(cfg *config) {

	type dep struct {
		on   []string      // Stay alive when at least one is alive.
		quit chan struct{} // Called to let source or sink die.
	}

	var (
		deps   = map[string]*dep{} // track dependencies.
		queues = map[string]*queue{}
		deaths = make(chan string)
	)

	// Start the consumers.
	for name, snk := range cfg.Sinks {
		q := newQueue()
		queues[name] = q
		c := newConsumer(snk, q)

		var on []string
	sources:
		for pname, src := range cfg.Sources {
			for _, s := range src.Sinks {
				if s == name {
					on = append(on, "p-"+pname)
					continue sources
				}
			}
		}

		cname := "c-" + name
		deps[cname] = &dep{on: on, quit: c.quit}
		go func() {
			defer func() { deaths <- cname }()
			c.run()
		}()
	}

	// Start the producers.
	for name, src := range cfg.Sources {
		cons := make([]*queue, 0, len(src.Sinks))
		on := make([]string, 0, len(src.Sinks))
		for _, snk := range src.Sinks {
			cons = append(cons, queues[snk])
			on = append(on, "c-"+snk)
		}

		p := newProducer(src, cons)
		pname := "p-" + name
		deps[pname] = &dep{on: on, quit: p.quit}
		go func() {
			defer func() { deaths <- pname }()
			p.run()
		}()
	}

	for name := range deaths {
		log.Debugf("Component %s died.", name)
		dep := deps[name]
		if dep == nil { // already dead -> ignore.
			continue
		}
		delete(deps, name)

	dependencies:
		for _, d := range dep.on {
			p := deps[d]
			if p == nil {
				continue
			}
			// Does one of the dependencies still live?
			for _, s := range p.on {
				if deps[s] != nil {
					continue dependencies
				}
			}
			// No -> let it die.
			log.Debugf("Request component %s to die.", d)
			go func() { p.quit <- struct{}{} }()
		}

		// End if all sources and sinks are gone.
		if len(deps) == 0 {
			break
		}
	}
}

func main() {

	cfgFile := flag.String("config", "config.toml", "TOML file with configuration")

	flag.Parse()

	cfg, err := loadConfig(*cfgFile)
	if err != nil {
		lg.Fatalf("error: %v.\n", err)
	}

	configureLogging(cfg)

	boot(cfg)
}
