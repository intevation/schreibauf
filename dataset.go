// Copyright 2018 by Siemens AG. All rights reserved.
// Engineering by Intevation GmbH.
// Use of this source code is governed by the Apache 2.0 license
// that can be found in the LICENSE file.

package main

import (
	"errors"
	"math"
	"strconv"
	"strings"
	"time"
)

// Latitude
// Longitude
// Altitude
// Event Type
// Error Angle
// Error Semi-major
// Error Semi-minor
// Time in Nanos
// Signal KA
// Alt Uncertainty
// Multiplicity
// CP Count
// Num Sensors
// DOF
// Chi-square
// Rise Time
// PTZ
// Max Rate Rise

const (
	controlIdx = iota
	latitudeIdx
	longitudeIdx
	altitudeIdx
	eventTypeIdx
	errorAngleIdx
	errorSemiMajorIdx
	errorSemiMinorIdx
	timeNanosIdx
	signalKAIdx
	altUncertaintyIdx
	multiplicityIdx
	cpCountIdx
	numSensorsIdx
	dofIdx
	chiSquareIdx
	riseTimeIdx
	ptzIdx
	maxRateRiseIdx
	partsSize // Keep at end as a sentinel!
)

type dataset struct {
	sourceID int

	control int
	raw     string

	latitude       float64
	longitude      float64
	altitude       float64
	eventType      int
	errorAngle     float64
	errorSemiMajor float64
	errorSemiMinor float64
	timeNanos      time.Time
	signalKA       float64
	altUncertainty float64
	multiplicity   float64
	cpCount        int
	numSensors     int
	dof            int
	chiSquare      float64
	riseTime       int64
	ptz            int64
	maxRateRise    float64
}

const (
	keepAliveLine = 6
	dataLine      = 8
)

var (
	errInvalidLine       = errors.New("invalid line")
	errUnknownLineFormat = errors.New("unknown line format")
)

func (ds *dataset) parse(raw string) error {

	ds.raw = raw

	parts := strings.Split(raw, ",")

	if len(parts) == 0 {
		return errInvalidLine
	}

	if err := parseInt(parts[0], &ds.control); err != nil {
		return err
	}

	// only parse data lines
	if ds.control != dataLine {
		return nil
	}

	if len(parts) < partsSize {
		return errInvalidLine
	}
	var err error

	pI := func(s string, addr *int) {
		if err == nil {
			err = parseInt(s, addr)
		}
	}
	pI64 := func(s string, addr *int64) {
		if err == nil {
			err = parseInt64(s, addr)
		}
	}
	pF64 := func(s string, addr *float64) {
		if err == nil {
			err = parseFloat64(s, addr)
		}
	}
	pTime := func(s string, addr *time.Time) {
		if err == nil {
			var t int64
			if err = parseInt64(s, &t); err == nil {
				*addr = time.Unix(0, t)
			}
		}
	}

	pF64(parts[latitudeIdx], &ds.latitude)
	pF64(parts[longitudeIdx], &ds.longitude)
	pF64(parts[altitudeIdx], &ds.altitude)
	pI(parts[eventTypeIdx], &ds.eventType)
	pF64(parts[errorAngleIdx], &ds.errorAngle)
	pF64(parts[errorSemiMajorIdx], &ds.errorSemiMajor)
	pF64(parts[errorSemiMinorIdx], &ds.errorSemiMinor)
	pTime(parts[timeNanosIdx], &ds.timeNanos)
	pF64(parts[signalKAIdx], &ds.signalKA)
	pF64(parts[altUncertaintyIdx], &ds.altUncertainty)
	pF64(parts[multiplicityIdx], &ds.multiplicity)
	pI(parts[cpCountIdx], &ds.cpCount)
	pI(parts[numSensorsIdx], &ds.numSensors)
	pI(parts[dofIdx], &ds.dof)
	pF64(parts[chiSquareIdx], &ds.chiSquare)
	pI64(parts[riseTimeIdx], &ds.riseTime)
	pI64(parts[ptzIdx], &ds.ptz)
	pF64(parts[maxRateRiseIdx], &ds.maxRateRise)

	return err
}

func parseInt(s string, d *int) error {
	v, err := strconv.Atoi(strings.TrimSpace(s))
	if err == nil {
		*d = v
	}
	return err
}

func parseInt64(s string, d *int64) error {
	v, err := strconv.ParseInt(strings.TrimSpace(s), 10, 64)
	if err == nil {
		*d = v
	}
	return err
}

func parseFloat64(s string, d *float64) error {
	v, err := strconv.ParseFloat(strings.TrimSpace(s), 64)
	if err == nil {
		*d = v
	}
	return err
}

func deg2rad(d float64) float64 {
	return d * math.Pi / 180.0
}

func (p *dataset) distanceVicenty(other *dataset) float64 {
	var (
		lat1 = p.latitude
		lon1 = p.longitude
		lat2 = other.latitude
		lon2 = other.longitude
	)
	const (
		a = 6378137
		b = 6356752.314245
		f = 1 / 298.257223563 // WGS-84 ellipsoid params
	)
	L := deg2rad(lon2 - lon1)
	U1 := math.Atan((1 - f) * math.Tan(deg2rad(lat1)))
	U2 := math.Atan((1 - f) * math.Tan(deg2rad(lat2)))
	sinU1 := math.Sin(U1)
	cosU1 := math.Cos(U1)
	sinU2 := math.Sin(U2)
	cosU2 := math.Cos(U2)

	lambda := L
	var lambdaP, cosSqAlpha, sinSigma, cos2SigmaM, cosSigma, sigma float64
	iterLimit := 100
	for {
		sinLambda := math.Sin(lambda)
		cosLambda := math.Cos(lambda)
		sinSigma = math.Sqrt(
			(cosU2*sinLambda)*(cosU2*sinLambda) +
				(cosU1*sinU2-sinU1*cosU2*cosLambda)*
					(cosU1*sinU2-sinU1*cosU2*cosLambda))
		if sinSigma == 0 {
			return 0 // co-incident points
		}
		cosSigma = sinU1*sinU2 + cosU1*cosU2*cosLambda
		sigma = math.Atan2(sinSigma, cosSigma)
		sinAlpha := cosU1 * cosU2 * sinLambda / sinSigma
		cosSqAlpha = 1 - sinAlpha*sinAlpha
		cos2SigmaM = cosSigma - 2*sinU1*sinU2/cosSqAlpha
		if math.IsNaN(cos2SigmaM) {
			cos2SigmaM = 0 // equatorial line: cosSqAlpha=0 (§6)
		}
		C := f / 16 * cosSqAlpha * (4 + f*(4-3*cosSqAlpha))
		lambdaP = lambda
		lambda = L + (1-C)*f*sinAlpha*
			(sigma+C*sinSigma*(cos2SigmaM+C*cosSigma*(-1+2*cos2SigmaM*cos2SigmaM)))

		if iterLimit--; iterLimit == 0 || math.Abs(lambda-lambdaP) <= 1e-12 {
			break
		}
	}

	if iterLimit == 0 {
		return math.NaN() // formula failed to converge
	}
	uSq := cosSqAlpha * (a*a - b*b) / (b * b)
	A := 1 + uSq/16384*(4096+uSq*(-768+uSq*(320-175*uSq)))
	B := uSq / 1024 * (256 + uSq*(-128+uSq*(74-47*uSq)))

	deltaSigma := B * sinSigma *
		(cos2SigmaM + B/4*(cosSigma*(-1+2*cos2SigmaM*cos2SigmaM)-B/6*
			cos2SigmaM*(-3+4*sinSigma*sinSigma)*(-3+4*cos2SigmaM*cos2SigmaM)))
	s := b * A * (sigma - deltaSigma)

	return s
}

func (p *dataset) distance(point *dataset) float64 {

	const EarthRadius = 6378137.0

	dLat := deg2rad(point.latitude - p.latitude)
	dLng := math.Abs(deg2rad(point.longitude - p.longitude))

	if dLng > math.Pi {
		dLng = 2*math.Pi - dLng
	}

	x := dLng * math.Cos(deg2rad((p.latitude+point.latitude)/2.0))
	return math.Sqrt(dLat*dLat+x*x) * EarthRadius
}

const spaceEps = 400

func (ds *dataset) equals(other *dataset) bool {
	return ds.sourceID != other.sourceID &&
		ds.eventType == other.eventType &&
		//		((ds.multiplicity == 0 && other.multiplicity == 0) ||
		//			(ds.multiplicity > 0 && other.multiplicity > 0)) &&
		//		((ds.cpCount == 0 && other.cpCount == 0) ||
		//			(ds.cpCount > 0 && other.cpCount > 0)) &&
		ds.distanceVicenty(other) < spaceEps
}
