// Copyright 2018 by Siemens AG. All rights reserved.
// Engineering by Intevation GmbH.
// Use of this source code is governed by the Apache 2.0 license
// that can be found in the LICENSE file.

package main

import (
	"database/sql"
	"time"

	"github.com/sirupsen/logrus"
)

type consumer struct {
	sink     *sink
	producer *queue
	hold     *dataset
	ring     *idDatasetRing

	sel *prepared
	ins *prepared
	up  *prepared

	quit chan struct{}
	done bool
}

func newConsumer(sink *sink, producer *queue) *consumer {
	var ring *idDatasetRing
	if !*sink.SQLDedup {
		ring = newIDDatasetRing(*sink.BufferSize)
	}
	return &consumer{
		sink:     sink,
		producer: producer,
		ring:     ring,
		quit:     make(chan struct{}),
	}
}

func (c *consumer) search(ds *dataset, db *sql.DB) (int64, error) {
	stmt, err := c.sel.prepare(ds.timeNanos.Year(), db)
	if err != nil {
		return 0, err
	}
	var id int64
	err = stmt.QueryRow(
		ds.eventType,
		ds.longitude, ds.latitude,
		ds.timeNanos,
		ds.multiplicity,
		ds.cpCount,
	).Scan(&id)
	return id, err
}

func (c *consumer) store(ds *dataset, db *sql.DB) (int64, error) {
	stmt, err := c.ins.prepare(ds.timeNanos.Year(), db)
	if err != nil {
		return 0, err
	}
	var id int64
	err = stmt.QueryRow(
		ds.timeNanos,
		ds.eventType,
		ds.signalKA,
		ds.longitude, ds.latitude,
		ds.errorSemiMajor,
		ds.errorSemiMinor,
		ds.errorAngle,
		ds.dof,
		ds.chiSquare,
		ds.ptz,
		ds.riseTime,
		ds.numSensors,
		ds.altitude,
		ds.sourceID,
		ds.maxRateRise,
		ds.altUncertainty,
		ds.multiplicity,
		ds.cpCount,
	).Scan(&id)

	if err == nil && log.Level >= logrus.DebugLevel {
		log.Debugf("Store id: %d, time: %s, lat: %.4f, lon: %.4f",
			id,
			ds.timeNanos.Format("2006-01-02T15:04:05.000Z07:00"),
			ds.latitude,
			ds.longitude,
		)
	}
	return id, err
}

func (c *consumer) dedupRing(ds *dataset, db *sql.DB) error {

	if old := c.ring.find(ds); old != nil { // seen before
		return c.rewrite(old.id, ds, db)
	}

	// new one
	id, err := c.store(ds, db)
	if err == nil {
		c.ring.insert(id, ds)
	}
	return err
}

func (c *consumer) rewrite(id int64, ds *dataset, db *sql.DB) error {
	stmt, err := c.up.prepare(ds.timeNanos.Year(), db)
	if err != nil {
		return err
	}
	value := c.sink.Rewrites.replace(ds.sourceID)
	_, err = stmt.Exec(value, id)
	if err == nil && log.Level >= logrus.DebugLevel {
		log.Debugf("Rewrote TLP of %d to %d.", id, value)
	}
	return err
}

func (c *consumer) dedupSQL(ds *dataset, db *sql.DB) error {

	old, err := c.search(ds, db)
	switch {
	case err == sql.ErrNoRows: // new one
		_, err := c.store(ds, db)
		return err
	case err != nil:
		return err
	}
	return c.rewrite(old, ds, db)
}

func (c *consumer) handle(db *sql.DB) error {
	defer db.Close()
	var dedup func(*dataset, *sql.DB) error
	if c.ring != nil {
		// pre-fill the ring buffer with the latest events from the db.
		if err := c.ring.prefill(db, *c.sink.TableName); err != nil {
			return err
		}
		dedup = c.dedupRing
	} else {
		dedup = c.dedupSQL
	}
	defer func() {
		c.sel.close()
		c.ins.close()
		c.up.close()
	}()

	src := make(chan *dataset)
	pdone := make(chan struct{})

	go func() {
		for {
			select {
			case src <- c.producer.Remove():
			case <-pdone:
				return
			}
		}
	}()
	defer close(pdone)

	for {
		var ds *dataset
		if c.hold != nil {
			ds = c.hold
			//log.Debug("Retry event from source %d.", c.hold.sourceID)
			c.hold = nil
		} else {
			select {
			case ds = <-src:
			case <-c.quit:
				c.done = true
				return nil
			}
		}
		if err := dedup(ds, db); err != nil {
			c.hold = ds
			return err
		}
	}
}

func (c *consumer) run() {

	dsn := pgDSN(*c.sink.Host, *c.sink.Port, *c.sink.Database,
		*c.sink.User, *c.sink.Password, *c.sink.SSL)

	c.sel = &prepared{prefix: *c.sink.TableName, tmpl: selectTmpl}
	c.ins = &prepared{prefix: *c.sink.TableName, tmpl: insertTmpl}
	c.up = &prepared{prefix: *c.sink.TableName, tmpl: updateTmpl}

	for !c.done {
		var db *sql.DB
		log.Infof("Connecting to PostgreSQL %s@%s:%d/%s ...",
			*c.sink.User, *c.sink.Host, *c.sink.Port, *c.sink.Database)
		for tries := 0; ; {
			select {
			case <-c.quit:
				c.done = true
			default:
			}
			if c.done {
				return
			}
			var err error

			if db, err = sql.Open("postgres", dsn); err == nil {
				if err = db.Ping(); err == nil {
					if err = c.handle(db); err != nil {
						log.Errorf("error: %v.", err)
					}
					continue
				}
			}
			log.Errorf("error: %v.", err)

			if *c.sink.MaxReconnects > 0 {
				if tries++; tries > *c.sink.MaxReconnects {
					log.Error("error: sink died.")
					return
				}
			}
			time.Sleep((*c).sink.ReconnectWait.Duration)
		}
	}
}
