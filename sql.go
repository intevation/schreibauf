// Copyright 2018 by Siemens AG. All rights reserved.
// Engineering by Intevation GmbH.
// Use of this source code is governed by the Apache 2.0 license
// that can be found in the LICENSE file.

package main

import (
	"database/sql"
	"fmt"
	"strconv"
	"strings"

	_ "github.com/lib/pq"
)

const (
	insertTmpl = `INSERT INTO %s (
  zeit,
  typ,
  strom,
  koordinate,
  grosseachse,
  kleineachse,
  winkelachse,
  freigrade,
  chiquadrat,
  steigzeit,
  spitzenull,
  sensorzahl,
  hoehe,
  tlp,
  maxraterise,
  hoeheungenau,
  strokezahl,
  pulsezahl
  )
VALUES (
  $1,
  $2,
  $3,
  ST_SetSRID(ST_MakePoint($4, $5), 4326),
  $6,
  $7,
  $8,
  $9,
  $10,
  $11,
  $12,
  $13,
  $14,
  $15,
  $16,
  $17,
  $18,
  $19)
RETURNING id`

	updateTmpl = `UPDATE %s SET tlp = $1 WHERE id = $2`

	selectTmpl = `SELECT id FROM %s WHERE
  typ = $1
  AND ( tlp = 1 OR tlp = 2 )
  AND ST_Distance(koordinate, ST_SetSRID(ST_MakePoint($2, $3), 4326)) < 0.04
  AND zeit BETWEEN $4::timestamp with time zone - '250 milliseconds'::interval AND
                   $4::timestamp with time zone + '250 milliseconds'::interval
  AND ((strokezahl = 0 AND $5 = 0) OR (strokezahl > 0 AND $5 > 0))
  AND ((pulsezahl = 0 AND $6 = 0)  OR (pulsezahl > 0 AND $6 > 0))`

	bootTmpl = `SELECT * FROM (
  SELECT
    id,
    typ,
    zeit,
    ST_X(koordinate) AS X,
    ST_Y(koordinate) AS Y,
    strokezahl,
    pulsezahl
  FROM %s ORDER BY zeit DESC LIMIT $1) last ORDER BY zeit`
)

type prepared struct {
	year   int
	prefix string
	tmpl   string
	stmt   *sql.Stmt
}

func (p *prepared) prepare(year int, db *sql.DB) (*sql.Stmt, error) {
	var err error
	if p.stmt == nil || p.year != year {
		p.close()
		p.year = year
		table := p.prefix + strconv.Itoa(year)
		smtSQL := fmt.Sprintf(p.tmpl, table)
		p.stmt, err = db.Prepare(smtSQL)
	}
	return p.stmt, err
}

func (p *prepared) close() error {
	if stmt := p.stmt; stmt != nil {
		p.stmt = nil
		return stmt.Close()
	}
	return nil
}

var pgQuote = strings.NewReplacer(`\`, `\\`, `'`, `\'`).Replace

func pgDSN(host string, port int, name, user, password, sslmode string) string {
	return fmt.Sprintf("host='%s' port=%d dbname='%s' user='%s' password='%s' sslmode=%s",
		pgQuote(host), port, pgQuote(name),
		pgQuote(user), pgQuote(password), sslmode)
}
