package main

import (
	"fmt"
	"testing"
	"time"
)

func TestTime(t *testing.T) {
	const (
		a = "2018-09-18 10:22:22.149788+02"
		b = "2018-09-18 10:22:22.149790+02"

		format = "2006-01-02 15:04:05.000000Z07"
	)

	t1, err := time.Parse(format, a)
	if err != nil {
		t.Fatalf("Invalid time: %v", err)
		return
	}
	t2, err := time.Parse(format, b)
	if err != nil {
		t.Fatalf("Invalid time: %v", err)
		return
	}

	before := t1.Add(-timeEps)
	after := t2.Add(+timeEps)

	if t2.Before(before) || t2.After(after) {
		t.Errorf("%v not in time interval", t1.Sub(t2))
	}
}

func TestEquals(t *testing.T) {
	d1 := dataset{
		latitude:  25.4967,
		longitude: 34.8165,
		eventType: 1,
		cpCount:   1,
	}
	d2 := dataset{
		latitude:  25.4981,
		longitude: 34.815,
		eventType: 1,
		cpCount:   1,
	}
	ids := newIDDatasetRing(4096)
	ids.insert(0, &d1)

	fmt.Println(ids.find(&d2) != nil)
}

func TestDistanceNew(t *testing.T) {

	points := [][2]float64{
		{25.4967, 34.8165},
		{25.4981, 34.815},
		{10.9397, 65.0663},
		{10.919, 65.0692},
		{10.9227, 65.07},
	}

	for i := range points {
		p1 := dataset{
			latitude:  points[i][1],
			longitude: points[i][0],
		}
		for j := i + 1; j < len(points); j++ {
			p2 := dataset{
				latitude:  points[j][1],
				longitude: points[j][0],
			}
			d1 := p1.distanceVicenty(&p2)
			d2 := p1.distance(&p2)
			fmt.Printf("distance %d - %d: %f %f\n", i, j, d1, d2)
		}
	}

}
func TestDistance(t *testing.T) {

	points := [][2][2]float64{
		{{8.6725, 40.8847}, {8.6732, 40.8844}},
		{{4.4905, 40.3748}, {4.4913, 40.3743}},
		{{5.1575, 47.0537}, {5.1621, 47.0568}},
	}

	for _, pair := range points {
		p1 := dataset{
			latitude:  pair[0][0],
			longitude: pair[0][1],
		}
		p2 := dataset{
			latitude:  pair[1][0],
			longitude: pair[1][1],
		}

		d := p1.distanceVicenty(&p2)
		fmt.Printf("distance %f\n", d)

	}
}
