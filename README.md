# schreibauf

A tool to read CSV like data streams from Vaisala Total Lightning Processors (TLPs)
and writes the deduplicated data to PostgreSQL databases.

## Build

You need a working [Go](http://golang.org) build environment (1.10+).

```
go get -u -v bitbucket.org/intevation/schreibauf
```

Place the resulting `schreibauf` binary into your `PATH`.

## Usage

See [usage.md](doc/usage.md) for details.

## License

This is Free Software covered by the Apache 2.0 license. See [LICENSE](LICENSE) for details.  
(c) 2018 by SIEMENS AG. Engineering by Intevation GmbH.
