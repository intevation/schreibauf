package main

import (
	"fmt"
	"math/rand"
	"testing"
	"time"
)

func linear(x1, y1, x2, y2, scale float64) (float64, float64) {
	x := x1 + scale*(x2-x1)
	y := y1 + scale*(y2-y1)
	return x, y
}

func nearby(d1, d2 *dataset, radius float64) {
	for i := 0; i < 10; i++ {
		dist := d1.distanceVicenty(d2)
		if dist < radius {
			break
		}
		d2.latitude, d2.longitude = linear(
			d1.latitude, d1.longitude,
			d2.latitude, d2.longitude, radius/dist)
	}
}

func TestPressure(t *testing.T) {

	const (
		max   = 1000000
		store = 1
		fetch = 2
	)

	r := newIDDatasetRing(4096)

	tm := time.Time{}

	rnd := rand.New(rand.NewSource(11))

	var hold *dataset
	count := 1

	lat := func() float64 { return rnd.Float64()*2*85 - 85 }
	lon := func() float64 { return rnd.Float64()*2*180 - 180 }

	var missed, hits int
	var id int64

	for i := 0; i < max; i++ {

		var what int

		if count--; count == 0 {

			if hold == nil {
				what = store
			} else {
				what = fetch
			}

			count = rnd.Intn(20) + 5
		}

		var ds *dataset

		switch what {
		case store:
			ds = &dataset{
				timeNanos: tm,
				latitude:  lat(),
				longitude: lon(),
			}
			hold = ds
		case fetch:
			nds := *hold
			nds.timeNanos = nds.timeNanos.Add(
				time.Duration(rnd.Int31n(2*125)-125) * time.Microsecond)
			nds.latitude, nds.longitude = lat(), lon()
			nearby(hold, &nds, 399)
			ds, hold = &nds, nil
		default:
			ds = &dataset{
				timeNanos: tm,
				latitude:  lat(),
				longitude: lon(),
			}
		}

		found := r.find(ds)
		if what == fetch {
			if found == nil {
				missed++
			} else {
				hits++
			}
		}
		if found == nil {
			r.insert(id, ds)
			id++
		}

		ms := time.Millisecond*time.Duration(rnd.Int63n(1000)) + 1
		tm = tm.Add(ms)
	}

	fmt.Printf("hits: %d\n", hits)
	fmt.Printf("missed: %d\n", missed)
	fmt.Printf("id: %d\n", id)

	if missed > 0 {
		t.Errorf("missed is expected to be zero")
	}

	if id+int64(hits) != max {
		t.Errorf("too many hits")
	}
}
