#!/usr/bin/env sh

go get -u -v github.com/BurntSushi/toml
go get -u -v github.com/lib/pq
go get -u -v github.com/sirupsen/logrus

