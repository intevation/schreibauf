// Copyright 2018 by Siemens AG. All rights reserved.
// Engineering by Intevation GmbH.
// Use of this source code is governed by the Apache 2.0 license
// that can be found in the LICENSE file.

package main

import (
	"fmt"
	"testing"
	"time"
)

func dump(r *idDatasetRing) {
	for i := range r.slots {
		x := &r.slots[i]
		var head string
		if r.head == i {
			head = " <--head"
		}
		fmt.Printf("%d: %v%s\n", x.id, x.dataset.timeNanos, head)
	}
}

func TestInsert(t *testing.T) {

	r := newIDDatasetRing(4)

	for i := int64(3); i >= 0; i-- {
		ds := dataset{timeNanos: time.Unix(i, 0)}
		r.insert(i, &ds)
	}

	for i := int64(0); i < 4; i++ {
		x := &r.slots[i]
		if ti := time.Unix(i, 0); !x.dataset.timeNanos.Equal(ti) {
			t.Errorf("index %d time fail %v %v.", i, x.dataset.timeNanos, ti)
		}
	}

	fmt.Println("Insert 4 ----")
	four := dataset{timeNanos: time.Unix(4, 0)}
	r.insert(4, &four)
	dump(r)
	fmt.Println("---")

	fmt.Println("Insert 0 ----")
	zero := dataset{timeNanos: time.Unix(0, 0)}
	r.insert(0, &zero)
	dump(r)

	fmt.Println("Insert 6 ----")
	six := dataset{timeNanos: time.Unix(6, 0)}
	r.insert(6, &six)
	dump(r)

	fmt.Println("Insert 5 ----")
	five := dataset{timeNanos: time.Unix(5, 0)}
	r.insert(5, &five)
	dump(r)
	fmt.Println("Insert 8 ----")
	eight := dataset{timeNanos: time.Unix(8, 0)}
	r.insert(8, &eight)
	dump(r)
	fmt.Println("Insert 7 ----")
	seven := dataset{timeNanos: time.Unix(7, 0)}
	r.insert(7, &seven)
	dump(r)
}
