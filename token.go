// Copyright 2018 by Siemens AG. All rights reserved.
// Engineering by Intevation GmbH.
// Use of this source code is governed by the Apache 2.0 license
// that can be found in the LICENSE file.

package main

import (
	"bytes"
	"encoding/json"
)

func generateTokenMessage(token string) ([]byte, error) {
	var buf bytes.Buffer
	var msg = struct {
		ID     int    `json:"id"`
		Stream string `json:"stream"`
	}{
		ID:     0,
		Stream: token,
	}
	err := json.NewEncoder(&buf).Encode(&msg)
	return buf.Bytes(), err
}
