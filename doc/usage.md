# Usage

```
./schreibauf --help
Usage of ./schreibauf:
  -config string
    	TOML file with configuration (default "config.toml")
```

An example TOML configuration may look like this:
```
log-file  = "/var/log/schreibauf.log"
log-level = "info"

[sources]

  [sources.first]
    id    = 1
    host  = "tlp1.example.com"
    port  = 8082
    key   = "1972c775-ffa4-4500-8c2c-2d0bde013671"
    sinks = ["lightning"]

  [sources.second]
    id    = 2
    host  = "tlp2.example.com"
    port  = 8082
    key   = "1d14a858-cf97-4246-9026-7a7ced1fa118"
    sinks = ["lightning"]

[sinks]

  [sinks.lightning]
    host       = "database.example.com"
    port       = 5432
    user       = "schreiber"
    password   = "schreiber"
    database   = "blitze"
    table-name = "blitze"
    rewrites = [{ id = 1, new = 4 }, { id = 2, new = 3 }]
```

`log-file` and `log-level` are optional.
If `log-file` is not set logging will go to STDERR.
`log-level` may have the values  `panic`, `fatal`, `error`, `warn`, `info` or `debug`
and defaults to `warn`.

`schreibauf` has the concept of sources and sinks.
Sources are the TLP to read from and sinks are the PostgreSQL databases
to write to. Therefore you find the respective credentials
like keys, users, password, hosts and ports in these sections.
The `table-name` will be suffixed with year of the lightning
event. In the example above an event in 2018 will end up in a
table with name `blitze2018`.

Sources report to one or more sinks. Each source must have an unique id
to identify it. The sinks to report to are a comma separated list
of sink names in the `sinks` attribute of source.

## Deduplication

The lightning events arrive in a serialized order in the sinks.
So there is a strict order of arrival. If a second source reports
the same event the first one will get an update to the `tlp` field
consulting the rewrite rules.

Each data set from a source carries the id of the source.
If we have the same event already in the database the id of the
new source is looked up in the `id` fields of the `rewrites` table
of the sink. If found the correspondin `new` field is used to
uodate the `tlp` field in the database. If the id is not found
the value stays the same.

To identify duplicates the following criteria are used:

* The same time of the event +- 1/64 of a second.
* The same coordinate with a radius of 0.04 meters.
* The same event type.

## Technical details

`schreibauf` supports two depublication modes.

1. ring buffer
2. SQL deduplication

The default and prefered mode is the ring buffer mode.
To enable the SQL depublication mode insert a
`sql-dedup = true` into the configuration of a sink.

In the ring buffer mode a history of the last 8192
lightning events are hold in memory pre-filled from
the database. Use `buffer-size = <value>` to configure
an other buffer size. Each time a duplicate is detected
the event in the database is updated. If an event
falls out of the buffer by being overwritten with
newer events it will be not duplicated.

In the SQL depublication mode before a new dataset
is inserted into the database it is looked in the
database for duplicates. This is expensive and is not
needed for the most cases as the duplicates are expected
to come in a timely manner.

Future versions of `schreibauf` may get a third mode
based on `UPSERT` (in PostgreSQL speak `INSERT ... ON CONFLICT`)
but at the moment it seems not possible to write the
conflict conditions in a form needed to detect the duplicates.
